# été mono

Font made with [Brutalita](https://brutalita.com/) for [été](https://gitlab.com/raphaelbastide/ete/).

## Specimen

![Specimen image](specimen/specimen-1.png)

An [online specimen](https://raphaelbastide.com/ete-mono/) is also available if you want to give the font a try.

## License

[SIL OFL](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)